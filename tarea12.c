#include <stdio.h>
int main(int argc, char const *argv[]) {

  int piezas=0;
  int menudeo=450;  //Pongo la constante que es el precio por menudeo
  int mayoreo=435;  //Tambien el precio por mayoreo para solo mutiplicarlo por la cantidad de piezas

  printf("¿Cuantas piezas se vendieron?: ");  //Pido que se ingrese la cantidad de piezas vendidas para poder hacer los cálculos
  scanf("%d", &piezas);

  if (piezas>=1 && piezas<=12) {      //Reviso si la cantidad de piezas esta entre 1 y 12
    if (piezas>=1 && piezas<=23) {    //Si es cierto y también es menor que 23 se toma el valor de menudeo
      printf("El salario es $%.2f\n",piezas*menudeo*0.05);  //Al ser cierto tambien se multiplica por el 5%
    }
  }else if (piezas>=13 && piezas<=30){  //Reviso si la cantidad de piezas esta entre 13 y 30
      if (piezas>=1 && piezas<=23) {    //Tambien si es menor que 23 se toma el precio de mayoreo
        printf("El salario es $%.2f\n",piezas*mayoreo*0.07);  //Al ser cierto tambien se multiplica por el 7% pero con el precio de menudeo
      } else if(piezas>=24){           //SI es mayor que 24 se toma el precio de mayoreo
        printf("El salario es $%.2f\n",piezas*mayoreo*0.07);  //Al ser cierto tambien se multiplica por el 7% pero con el precio de mayoreo
      }
    }
    else if (piezas>30){    //Reviso si la cantidad de piezas es mayor que 30
      printf("El salario es $%.2f\n",piezas*mayoreo*0.10);  //Al ser cierto tambien se multiplica por el 10% con el precio de mayoreo
    }

  return 0;
}
